package com.example.retrofitapi;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.widget.TextView;

import retrofit2.Call;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private TextView mJsonTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mJsonTxtView = findViewById(R.id.mJsonTxtView);
        getPosts();
    }
    private void getPosts() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.etnassoft.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LibroService libroService = retrofit.create(LibroService.class);
        Call< List<Libro>> call = libroService.getPost();
        call.enqueue(new Callback<List<Libro>>() {
            @Override
            public void onResponse(Call<List<Libro>> call, Response<List<Libro>> response) {
                if (!response.isSuccessful()){
                    mJsonTxtView.setText("codigo:"+response.code());
                    return;
                }
                List<Libro> postsList = response.body();

                for(Libro libro : response.body()) {
                    String content="";
                    content += "ID:"+ libro.getID()+"\n";
                    content += "title:"+ libro.getTitle()+"\n";
                    content += "author:"+ libro.getAuthor()+"\n";
                    content += "content_short:"+ libro.getContent_short()+"\n";
                    content += "publisher:"+ libro.getPublisher()+"\n";
                    content += "publisher_date:"+ libro.getPublisher_date()+"\n";
                    content += "pages:"+ libro.getPages()+"\n";
                    content += "language:"+ libro.getLanguage()+"\n";
                    content += "url_details:"+ libro.getUrl_details()+"\n";
                    content += "url_download:"+ libro.getUrl_download()+"\n";
                    content += "url_read_online:"+ libro.getUrl_read_online()+"\n";
                    content += "cover:"+ libro.getCover()+"\n\n";
                    mJsonTxtView.append(content);
                }
                // arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Libro>> call, Throwable t) {
                mJsonTxtView.setText(t.getMessage());
            }
        });
    }
}