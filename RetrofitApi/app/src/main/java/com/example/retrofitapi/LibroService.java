package com.example.retrofitapi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LibroService {
    String API_ROUTE = "/api/v1/get/?all";
    @GET(API_ROUTE)
    Call<List<Libro>> getPost();
}
